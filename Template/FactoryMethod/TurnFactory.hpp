#ifndef TURNFACTORY_HPP
#define TURNFACTORY_HPP

#include "mainwindow.h"

#include "Game/Player/IPlayer.hpp"
#include "Template/Turn.hpp"
#include <memory>

class TurnFactory
{
public:
    TurnFactory(MainWindow *ui);

    std::shared_ptr<Turn> CreateTurn(PlayerType playerType, std::shared_ptr<IPlayer> attacker);

private:
    // HumanTurn needs the selected field coordinates
    MainWindow *m_ui;
};

#endif // TURNFACTORY_HPP
