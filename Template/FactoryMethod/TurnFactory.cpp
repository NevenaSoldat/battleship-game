#include "mainwindow.h"

#include "TurnFactory.hpp"
#include "Template/TurnTypes/HumanTurn.hpp"
#include "Template/TurnTypes/ComputerTurn.hpp"

TurnFactory::TurnFactory(MainWindow *ui)
    : m_ui(ui)
{

}

std::shared_ptr<Turn> TurnFactory::CreateTurn(PlayerType playerType, std::shared_ptr<IPlayer> attacker)
{
    std::shared_ptr<Turn> turn;
    switch(playerType)
    {
        case PlayerType::Computer:
            turn = std::make_shared<ComputerTurn>(attacker);
            break;
        case PlayerType::Human:
            turn = std::make_shared<HumanTurn>(m_ui);
            break;
        default:
            break;
    }

    return turn;
}
