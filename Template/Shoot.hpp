#ifndef SHOOT_HPP
#define SHOOT_HPP

#include "Game/Board/Position.hpp"
#include <memory>

class Shoot
{
public:
    Shoot(int x, int y);
    std::shared_ptr<Position> getPosition();

private:
    std::shared_ptr<Position> m_position;

};

#endif // SHOOT_HPP
