#include "Template/Shoot.hpp"

Shoot::Shoot(int x, int y) :
    m_position(std::make_shared<Position>(x, y, PositionStatus::Unknown, AvailabilityStatus::Available))
{}

std::shared_ptr<Position> Shoot::getPosition()
{
    return m_position;
}
