#include "ShipFactory.hpp"
#include "Game/Ships/ShipTypes/Carrier.hpp"
#include "Game/Ships/ShipTypes/Cruiser.hpp"
#include "Game/Ships/ShipTypes/Destroyer.hpp"
#include "Game/Ships/ShipTypes/Submarine.hpp"
#include "Game/Ships/ShipTypes/Battleship.hpp"

ShipFactory::ShipFactory()
{}

std::shared_ptr<IShip> ShipFactory::CreateShip(ShipType shipType, QVector<std::shared_ptr<Position>> shipCoordinates)
{
    std::shared_ptr<IShip> pShip;
    switch (shipType) {
        case ShipType::CarrierType:
            pShip = std::make_shared<Carrier>(shipCoordinates);
            break;
        case ShipType::CruiserType:
            pShip = std::make_shared<Cruiser>(shipCoordinates);
            break;
        case ShipType::DestroyerType:
            pShip = std::make_shared<Destroyer>(shipCoordinates);
            break;
        case ShipType::SubmarineType:
            pShip = std::make_shared<Submarine>(shipCoordinates);
            break;
        case ShipType::BattleshipType:
            pShip = std::make_shared<Battleship>(shipCoordinates);
            break;
    }

    return pShip;
}
