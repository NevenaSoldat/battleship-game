#ifndef SHIPFACTORY_HPP
#define SHIPFACTORY_HPP

#include "Game/Ships/IShip.hpp"
#include "Game/Board/Position.hpp"
#include <QVector>
#include <memory>

class ShipFactory
{
public:
    ShipFactory();

    std::shared_ptr<IShip> CreateShip(ShipType shipType, QVector<std::shared_ptr<Position>> shipCoordinates);
};


#endif // SHIPFACTORY_HPP
