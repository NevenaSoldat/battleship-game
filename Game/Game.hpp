#ifndef GAME_HPP
#define GAME_HPP

#include "mainwindow.h"

#include "IGame.hpp"
#include "Template/FactoryMethod/TurnFactory.hpp"

class Game : public IGame
{
public:
    Game(MainWindow *ui, std::shared_ptr<IPlayer> player1, std::shared_ptr<IPlayer> player2);
    bool AttackBy(std::shared_ptr<IPlayer> player, std::shared_ptr<IPlayer> defender) override;
    GameState GetGameState() override;

private:
    std::shared_ptr<IPlayer> m_attacker;
    std::shared_ptr<IPlayer> m_defender;
    GameState m_state;
    int m_round;

    std::shared_ptr<TurnFactory> m_turnFactory;
    QVector<std::shared_ptr<Turn>> m_Turns;
};

#endif // GAME_HPP
